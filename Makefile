#!/usr/bin/make

# Make file for Piscary Club website
# Copyright (c) 2013 Peter Harpending. <pharpend2@gmail.com>

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version. 

# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details. 

# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>. 

dist_dir := dist
source_dir := src
target_dir := build

build: setup general html scss css vnd

general:
	cp $(dist_dir)/README $(target_dir)/README
	cp $(dist_dir)/COPYING $(target_dir)/COPYING

css:
	cp -rf $(source_dir)/code/css/*.css $(target_dir)/css

html:
	cp -rf $(source_dir)/code/html/*.html $(target_dir)/

setup:
	mkdir -p $(target_dir)/
	mkdir -p $(target_dir)/css/
	mkdir -p $(target_dir)/js/
	mkdir -p $(target_dir)/vnd/
	
scss: 
	sass --update $(source_dir)/code/scss/:$(target_dir)/css --style compressed

vnd:
	cp -rf $(source_dir)/vnd $(target_dir)/vnd
