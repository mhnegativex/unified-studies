Copyright (c) 2013 Harrison Unruh. <htrain9000@gmail.com>     
Copyright (c) 2013 Peter Harpending. <pharpend2@gmail.com>

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.  This file is offered as-is, without any
warranty.

# Introduction

This is the site source tree of the website for Piscary Club at West
High School in Salt Lake City, UT, United States.

The lead developer is Harrison Unruh <htrain9000@gmail.com>. Email him
with any questions.

The bismuth developer is Peter Harpending <pharpend2@gmail.com>.

# Stuff we use

We are currently in the planning stage. 

We are using the following things:

* The [Bootstrap 3.0][4] CSS library.
* [Syntactically awesome stylesheets][5].
* [JQuery][7].

We are tentatively planning on using all of the following things:

* [PHP][2] with the [Drupal][3] library.
* [PHP's clone of JUnit][6].
* [PHP's clone of JavaDoc][8].

We have yet to implement any PHP into the site, hence, all of the PHP
stuff is hypothetical.

We will eventually publish coding standards and program skeletons.

# Building

Use the Makefile to build from the source. Edit the variable
`target_dir` to be the directory in which you want to build the site.
The compilation defaults to the `build` directory in this repository. 

* `make` or `make build` will do everything automagically.
* `make setup` will make all of the appropriate directories.
* `make general` will copy over the README, and the GPL.
* `make html` will copy over the HTML.
* `make css` will copy over the CSS into the app

**IMPORTANT -- If you attempt to run the code directly from the `src`
directory, it will not work. The code is designed to be run from a
separate build directory.**.

# Licensing

This code is licensed under the
[GNU General Public License, version 3][9]. A copy of the GNU General
Public License can be found in the COPYING.txt file in this repository.

# Contributing

This is a public Git repository. As such, anyone is welcome to
contribute, as long as your stuff doesn't suck. 

We use the [fork-and-pull workflow][1]. If you aren't familiar with
fork-and-pull, click on the link, and read the page.

[1]: https://help.github.com/articles/fork-a-repo
[2]: https://en.wikipedia.org/wiki/PHP
[3]: http://drupal.com/
[4]: http://getbootstrap.com/
[5]: http://sass-lang.com/
[6]: http://phpunit.sourceforge.net/
[7]: http://jquery.com/
[8]: https://en.wikipedia.org/wiki/PHPDoc
[9]: https://www.gnu.org/licenses/gpl.html
