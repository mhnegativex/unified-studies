**PHP standards for Piscary web project**

Version 0.0, 10 October 2013.

Copyright (c) 2013 Peter Harpending. <<pharpend2@gmail.com>>

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.  This file is offered as-is, without any
warranty.

## Introduction

This is a draft of the coding standards for SCSS. 

## Naming conventions

Unless there is a good reason not to (read: Bootstrap), all of our names will
follow the `lower-case-with-hyphens` convention. 

## Comments

All comments in SCSS need to be one-line comments. The only exception is the
copyright notice at the top of every file. That is a block comment, always.

The reason is, CSS does not support one-line comments. Hence, when the SCSS is
compiled into plain CSS, the one line comments are removed. The comments can
only cause trouble, so they are to be slaughtered.

## Indentation

All indents are four spaces.

## Braces

Opening braces start on the same line as the block definition. Closing braces go
on their own line.

Every closing brace, or other such parenthetical, closing a block of or
exceeding five lines in length, or whose block nests another braced,
angle-bracketed, or otherwise parenthesized block, must immediately be followed
by a comment indicating what the brace, bracket, or other parenthetical was
closing.
